import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import SalesPersonsForm from './SalesPersonsForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordList';
import SalesPersonsList from './SalesPersonsList';
import SalesPersonsHistory from './SalesPersonsHistory';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import AppointmentHistory from './AppointmentHistory';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';

function App() {
  const [automobile, setAutomobile] = useState([])
  const [salesPersonz, setSalesPersonz] = useState([])
  const [customers, setCustomers] = useState([])
  const [salesRecords, setSalesRecords] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [models, setModels] = useState([])
  const [appointments, setAppointments] = useState([])
  const [technicians, setTechnicians] = useState([])
  const [manufacturers, setManufacturers] = useState([])
  
  

  const fetchAutomobiles = async () => {
    const url =  "http://localhost:8100/api/automobiles/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setAutomobiles(data.autos)
    }
  }

  const fetchSalesRecord = async () => {
    const url =  "http://localhost:8090/api/sales_record/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setSalesRecords(data.sales_record)
    }
  }

  const fetchAutomobile = async () => {
    const url =  "http://localhost:8090/api/automobile/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setAutomobile(data.automobile)
    }
  }

  const fetchSalesPersons = async () => {
    const url =  "http://localhost:8090/api/sales_persons/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setSalesPersonz(data.sales_persons)
    }
  }

  const fetchCustomer = async () => {
    const url =  "http://localhost:8090/api/customer/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setCustomers(data.customer)
    }
  }

  const fetchAppointments = async () => {
  const url = "http://localhost:8080/api/appointments/"
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setAppointments(data.appointments)
    }
  }

  const fetchTechnicians = async () => {
  const url = "http://localhost:8080/api/technicians/"
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setTechnicians(data.technicians)
    }
  }

  const fetchManufacturers = async () => {
  const url = "http://localhost:8100/api/manufacturers/"
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setManufacturers(data.manufacturers)
  }
}

  const fetchVehicleModels = async () => {
  const url = "http://localhost:8100/api/models/"
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setModels(data.models)
  }
}


  useEffect(() => {
    fetchAppointments();
    fetchTechnicians();
    fetchManufacturers();
    fetchVehicleModels();
    fetchAutomobile();
    fetchSalesPersons();
    fetchCustomer();
    fetchSalesRecord();
    fetchAutomobiles();
    fetchVehicleModels();
  }, [setAppointments, setTechnicians, setManufacturers, setModels]);

  if (appointments === undefined) {
    return null;
  } else if (technicians === undefined) {
    return null;
  } else if (manufacturers === undefined) {
    return null;
  } else if (models === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales_persons/new" element={<SalesPersonsForm fetchSalesPersons={fetchSalesPersons} />} />
          <Route path="customer/new" element={<CustomerForm fetchCustomer={fetchCustomer} />} />
          <Route path="sales_record/new" element={<SalesRecordForm fetchAutomobile={fetchAutomobile} fetchSalesPersons={fetchSalesPersons} fetchCustomer={fetchCustomer} fetchSalesRecord={fetchSalesRecord} customers={customers} salesPersonz={salesPersonz} automobile={automobile} />} />
          <Route path="sales_record/list" element={<SalesRecordList fetchSalesRecord={fetchSalesRecord} salesRecords={salesRecords}/>} />
          <Route path="sales_persons/list" element={<SalesPersonsList fetchSalesPersons={fetchSalesPersons} salesPersonz={salesPersonz} />} />
          <Route path="sales_persons/history" element={<SalesPersonsHistory salesPersonz={salesPersonz} />} />
          <Route path="automobiles/list" element={<AutomobileList fetchAutomobiles={fetchAutomobiles} automobiles={automobiles} />} />
          <Route path="automobiles/new" element={<AutomobileForm fetchVehicleModels={fetchVehicleModels} fetchAutomobiles={fetchAutomobiles} models={models} />} />
          <Route path="appointments/" element={<AppointmentList fetchAppointments={fetchAppointments} appointments={appointments} />} />
          <Route path="appointments/new" element={<AppointmentForm fetchAppointments={fetchAppointments} technicians={technicians} />} />
          <Route path="appointments/search" element={<AppointmentHistory fetchAppointments={fetchAppointments} appointments={appointments} />} />
          <Route path="technicians/new" element={<TechnicianForm fetchTechnicians={fetchTechnicians} technicians={technicians} />} />
          <Route path="technicians" element={<TechnicianList fetchTechnicians={fetchTechnicians} technicians={technicians} />} />
          <Route path="manufacturers" element={<ManufacturerList fetchManufactures={fetchManufacturers} manufacturers={manufacturers} />} />
          <Route path="manufacturers/new" element={<ManufacturerForm fetchManufacturers={fetchManufacturers} manufacturers={manufacturers} />} />
          <Route path="models" element ={<VehicleModelList fetchVehicleModels={fetchVehicleModels} fetchManufacturers={fetchManufacturers} models={models} />} />
          <Route path="models/new" element = {<VehicleModelForm fetchVehicleModels={fetchVehicleModels} fetchManufacturers={fetchManufacturers} manufacturers={manufacturers} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
