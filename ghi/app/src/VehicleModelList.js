import React from 'react'

function VehicleModelList({models}) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Picture</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{ model.name }</td>
                  <td>
                    <img src={ model.picture_url } alt="" width="80" height="80" />
                  </td>
                  <td>{ model.manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default VehicleModelList