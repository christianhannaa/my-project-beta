import React from 'react'

function SalesPersonsList({salesPersonz}) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee Number</th>
            </tr>
          </thead>
          <tbody>
            {salesPersonz.map(salesPersons => {
              return (
                <tr key={salesPersons.id}>
                  <td>{ salesPersons.name }</td>
                  <td>{ salesPersons.employee_number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default SalesPersonsList
