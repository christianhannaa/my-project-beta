import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "customer/new">New customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="manufacturers/new">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="manufacturers">List of Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="models/new">Create a Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="models">List of Vehicle Models</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "automobiles/new/">New automobile</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "automobiles/list/">Automobiles list</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "sales_persons/new/">New sales person</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "sales_persons/list">Sales people</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "sales_persons/history">Sales person's history</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "sales_record/new">New sale</NavLink>
            </li>
            <li className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link active" aria-current="page" to= "sales_record/list">All sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="technicians/new">Create a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="technicians">List of Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="appointments/new">Create an Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="appointments">List of Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="appointments/search">Search VIN History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
